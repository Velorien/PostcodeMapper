﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostcodeMapping
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionstring = "YOUR-CONNECTION-STRING-GOES-HERE";
            try
            {
                string[] localAuthorities, postcodes;
                string temp;
                SqlCommand command;
                var conn = new SqlConnection();
                conn.ConnectionString = connectionstring;
                conn.Open();

                using (var reader = File.OpenText("la.txt"))
                {
                    localAuthorities = reader.ReadToEnd().Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                }

                using (var reader = File.OpenText("postcodes.txt"))
                {
                    postcodes = reader.ReadToEnd().Split(new[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
                }

                string sql = "insert into Postcodes(Postcode, LocalAuthority) values";

                StringBuilder builder = new StringBuilder(sql);
                for (int i = 0; i < localAuthorities.Length; i++)
                {
                    builder.Append($" ('{postcodes[i]}', '{localAuthorities[i]}'),");

                    if (i % 500 == 0 && i > 0)
                    {
                        Console.WriteLine("Done: " + i);
                        temp = builder.ToString();
                        temp = temp.Substring(0, temp.Length - 1);
                        builder = new StringBuilder(sql);
                        command = new SqlCommand(temp, conn);
                        command.ExecuteNonQuery();
                    }
                }

                temp = builder.ToString();
                temp = temp.Substring(0, temp.Length - 1);
                builder = new StringBuilder(sql);
                command = new SqlCommand(temp, conn);
                command.ExecuteNonQuery();

                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("done");
            Console.ReadKey();
        }
    }
}
